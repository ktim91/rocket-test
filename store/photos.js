export const state = () => ({
    photos: []
})

export const mutations = {
    setPhotos(state, photos){
        state.photos = photos
    }
}

export const actions = {
    async fetch({commit}){
        var per_page = 30,
            page = 1;
        const photos = await this.$axios.$get('https://api.unsplash.com/photos/?client_id=979694c91b05ca592589d2875b90c9f152e301942ba17801330ed2f3430c102f', 
            {
                params: {
                    order_by: 'popular',
                    per_page: per_page,
                    page: page++,
                }
            }
        )

        commit('setPhotos', photos)
    },
    async loadMore({commit}){
        var per_page = 30,
            page = 2;
        const photos = await this.$axios.$get('https://api.unsplash.com/photos/?client_id=979694c91b05ca592589d2875b90c9f152e301942ba17801330ed2f3430c102f', 
            {
                params: {
                    order_by: 'popular',
                    per_page: per_page,
                    page: page++,
                }
            }
        )

        commit('setPhotos', photos)
    }
}

export const getters = {
    photos: s => s.photos
}